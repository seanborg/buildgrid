# Copyright (C) 2018 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# pylint: disable=redefined-outer-name


import tempfile
import time
from threading import Thread
import uuid
from unittest import mock

import grpc
from grpc._server import _Context
import pytest
import testing.postgresql

from buildgrid._enums import OperationStage
from buildgrid._protos.build.bazel.remote.execution.v2 import remote_execution_pb2
from buildgrid._protos.google.longrunning import operations_pb2

from buildgrid.utils import create_digest
from buildgrid.server import job
from buildgrid.server.controller import ExecutionController
from buildgrid.server.cas.storage import lru_memory_cache
from buildgrid.server.actioncache.caches.lru_cache import LruActionCache
from buildgrid.server.execution import service
from buildgrid.server.execution.service import ExecutionService
from buildgrid.server.persistence.mem.impl import MemoryDataStore
from buildgrid.server.persistence.sql.impl import SQLDataStore
from buildgrid.server.persistence.sql import models
from buildgrid.server.metrics_names import (
EXECUTE_SERVICER_TIME_METRIC_NAME,
EXECUTE_REQUEST_COUNT_METRIC_NAME,
WAIT_EXECUTION_SERVICER_TIME_METRIC_NAME,
WAIT_EXECUTION_REQUEST_COUNT_METRIC_NAME,
)
from buildgrid.server.monitoring import _MonitoringBus
from tests.utils.metrics import mock_create_timer_record, mock_create_counter_record
from tests.utils.mocks import (
    MOCK_CORRELATED_INVOCATIONS_ID,
    MOCK_INVOCATION_ID,
    MOCK_TOOL_NAME,
    MOCK_TOOL_VERSION,
    mock_extract_request_metadata
)


server = mock.create_autospec(grpc.server)

command = remote_execution_pb2.Command()
command.platform.properties.add(name="OSFamily", value="linux")
command.platform.properties.add(name="ISA", value="x86")
command_digest = create_digest(command.SerializeToString())

action = remote_execution_pb2.Action(command_digest=command_digest,
                                     do_not_cache=True)
action_digest = create_digest(action.SerializeToString())

try:
    Postgresql = testing.postgresql.PostgresqlFactory(cache_initialized_db=True)
except RuntimeError:
    print(f"skipping all POSTGRES tests")
    POSTGRES_INSTALLED = False
else:
    POSTGRES_INSTALLED = True

postgres_skip_message = "Skipped test due to Postgres not being installed"
check_for_postgres = pytest.mark.skipif(not POSTGRES_INSTALLED, reason=postgres_skip_message)


@pytest.fixture
def context():
    cxt = mock.MagicMock(spec=_Context)
    yield cxt


PARAMS = [(impl, use_cache) for impl in ["sqlite", "postgresql", "mem"]
          for use_cache in ["action-cache", "no-action-cache"]]


@pytest.fixture(params=("sqlite", "postgresql", "mem"))
def datastore(request):
    impl = request.param
    storage = lru_memory_cache.LRUMemoryCache(1024 * 1024)

    write_session = storage.begin_write(command_digest)
    write_session.write(command.SerializeToString())
    storage.commit_write(command_digest, write_session)

    write_session = storage.begin_write(action_digest)
    write_session.write(action.SerializeToString())
    storage.commit_write(action_digest, write_session)
    if impl == "sqlite":
        with tempfile.NamedTemporaryFile() as db:
            yield SQLDataStore(
                storage,
                connection_string=f"sqlite:///{db.name}",
                automigrate=True
            )
    elif impl == "postgresql":
        with Postgresql() as postgresql:
            yield SQLDataStore(
                storage,
                connection_string=postgresql.url(),
                automigrate=True
            )
    elif impl == "mem":
        yield MemoryDataStore(storage)


@pytest.fixture(params=("action-cache", "no-action-cache"))
def controller(request, datastore):
    use_cache = request.param
    storage = datastore.storage

    try:
        keys = ['OSFamily', 'ISA']
        if use_cache == "action-cache":
            cache = LruActionCache(storage, 50)
            yield ExecutionController(
                datastore, storage=storage, action_cache=cache,
                property_keys=keys, match_properties=keys)
        else:
            yield ExecutionController(
                datastore, storage=storage,
                property_keys=keys, match_properties=keys)
    finally:
        datastore.watcher_keep_running = False


# Instance to test
@pytest.fixture()
def instance(controller, request):
    with mock.patch.object(service, 'remote_execution_pb2_grpc'):
        execution_service = ExecutionService(server)
        execution_service.add_instance("", controller.execution_instance)
        yield execution_service

@pytest.mark.parametrize("skip_cache_lookup", [True, False])
@mock.patch('buildgrid.server.metrics_utils.create_timer_record', new=mock_create_timer_record)
@mock.patch('buildgrid.server.metrics_utils.create_counter_record', new=mock_create_counter_record)
@mock.patch('buildgrid.server.request_metadata_utils.extract_request_metadata', new=mock_extract_request_metadata)
@check_for_postgres
def test_execute(skip_cache_lookup, instance, context):
    _MonitoringBus._instance = mock.Mock()
    scheduler = instance.get_scheduler("")

    request = remote_execution_pb2.ExecuteRequest(instance_name='',
                                                  action_digest=action_digest,
                                                  skip_cache_lookup=skip_cache_lookup)
    response = instance.Execute(request, context)

    result = next(response)
    assert isinstance(result, operations_pb2.Operation)
    metadata = remote_execution_pb2.ExecuteOperationMetadata()
    result.metadata.Unpack(metadata)
    assert metadata.stage == OperationStage.QUEUED.value
    operation_uuid = result.name.split('/')[-1]
    assert uuid.UUID(operation_uuid, version=4)
    assert result.done is False

    if isinstance(scheduler.data_store, SQLDataStore):
        with scheduler.data_store.session() as session:
            operation = session.query(models.Operation).filter_by(name=operation_uuid).one()
            assert operation.tool_name == MOCK_TOOL_NAME
            assert operation.tool_version == MOCK_TOOL_VERSION
            assert operation.invocation_id == MOCK_INVOCATION_ID
            assert operation.correlated_invocations_id == MOCK_CORRELATED_INVOCATIONS_ID

    def _complete_jobs():
        # This is pretty horrible, but we sleep here to allow a delay between starting
        # the thread and sending the completion update.
        #
        # This gives the rest of the test time to call the iterator again and trigger
        # the wait for the update notification properly.
        time.sleep(0.1)
        for job_name in scheduler.data_store.watched_jobs.keys():
            scheduler._update_job_operation_stage(job_name, OperationStage.COMPLETED)

    # Update the watched job in a thread. This allows us to immediately move on
    # to waiting for an operation update message in the main test thread. This
    # is both
    #   (a) more representative of how the Execution service is used in reality
    #   (b) necessary to avoid a race in the way we handle notification of job
    #       updates.
    #
    # In brief, the Execute request handler doesn't start listening for updates until
    # the second `next()` call to the iterator of operation messages. If we accidentally
    # miss the completion change when its also the second operation message (so the
    # first operation message triggered by waiting for changes), the request handler
    # thread will hang indefinitely waiting for an update that will never arrive.
    #
    # This could be avoided by looking in the event history and triggering an extra
    # message if there is a chance we missed some, but this makes WaitExecution
    # behave strangely just so that this contrived test scenario can work smoothly.
    job_completer = Thread(target=_complete_jobs)
    job_completer.start()

    try:
        # We're trying to exhaust the iterator here to finish the Execute request handler.
        # This should finish after two passes, the first to relay the operation message
        # containing the completion information, and the second to hit the end and raise
        # StopIteration.
        #
        # A plain for loop is used in lieu of calling `next()` twice explicitly to make it
        # less likely that a future change needs this test to be rewritten.
        #
        # There's a chance of looping indefinitely here, if the operation updates stream
        # gets broken somehow, but that is something we want to catch by breaking lots of
        # our tests anyway.
        for message in response:
            pass
    except StopIteration:
        # Good, this is what we wanted to happen. We need to use up the whole generator
        # here so that the duration metric actually gets published.
        pass

    # Asserting that metrics were collected:
    mock_timing_record = mock_create_timer_record(name=EXECUTE_SERVICER_TIME_METRIC_NAME)
    mock_counter_record = mock_create_counter_record(name=EXECUTE_REQUEST_COUNT_METRIC_NAME,
                                                     metadata={'instance-name': ''},
                                                     count=1)

    call_list = [mock.call(mock_timing_record), mock.call(mock_counter_record)]
    _MonitoringBus._instance.send_record_nowait.assert_has_calls(call_list, any_order=True)


@check_for_postgres
def test_wrong_execute_instance(instance, context):
    request = remote_execution_pb2.ExecuteRequest(instance_name='blade')
    response = instance.Execute(request, context)

    next(response)
    context.set_code.assert_called_once_with(grpc.StatusCode.INVALID_ARGUMENT)


@check_for_postgres
def test_no_action_digest_in_storage(instance, context):
    request = remote_execution_pb2.ExecuteRequest(instance_name='',
                                                  skip_cache_lookup=True)
    response = instance.Execute(request, context)

    next(response)
    context.set_code.assert_called_once_with(grpc.StatusCode.FAILED_PRECONDITION)

@mock.patch('buildgrid.server.metrics_utils.create_timer_record', new=mock_create_timer_record)
@mock.patch('buildgrid.server.metrics_utils.create_counter_record', new=mock_create_counter_record)
@check_for_postgres
def test_wait_execution(instance, controller, context):
    scheduler = controller.execution_instance._scheduler

    job_name = scheduler.queue_job_action(action, action_digest, skip_cache_lookup=True)

    operation_name = controller.execution_instance.register_job_peer(job_name,
                                                                     context.peer())

    scheduler._update_job_operation_stage(job_name, OperationStage.COMPLETED)

    _MonitoringBus._instance = mock.Mock()

    request = remote_execution_pb2.WaitExecutionRequest(name=operation_name)

    response = instance.WaitExecution(request, context)

    result = next(response)

    assert isinstance(result, operations_pb2.Operation)
    metadata = remote_execution_pb2.ExecuteOperationMetadata()
    result.metadata.Unpack(metadata)
    assert metadata.stage == job.OperationStage.COMPLETED.value
    assert result.done is True

    if isinstance(scheduler.data_store, SQLDataStore):
        with scheduler.data_store.session() as session:
            record = session.query(models.Job).filter_by(name=job_name).first()
            assert record is not None
            assert record.stage == job.OperationStage.COMPLETED.value
            assert record.operations
            assert all(op.done for op in record.operations)
            assert record.worker_completed_timestamp is not None

    try:
        # This should raise StopIteration. If something changes in the internals of the
        # operation update streaming, this may hang indefinitely.
        next(response)
    except StopIteration:
        # Good, this is what we wanted to happen. We need to use up the whole generator
        # here so that the duration metric actually gets published.
        pass

    controller.execution_instance.unregister_operation_peer(operation_name, context.peer())

    # Asserting that metrics were collected:
    mock_timing_record = mock_create_timer_record(name=WAIT_EXECUTION_SERVICER_TIME_METRIC_NAME)
    mock_counter_record = mock_create_counter_record(name=WAIT_EXECUTION_REQUEST_COUNT_METRIC_NAME,
                                                     metadata={'instance-name': ''},
                                                     count=1)

    call_list = [mock.call(mock_timing_record), mock.call(mock_counter_record)]
    _MonitoringBus._instance.send_record_nowait.assert_has_calls(call_list, any_order=True)


@check_for_postgres
def test_wrong_instance_wait_execution(instance, context):
    request = remote_execution_pb2.WaitExecutionRequest(name="blade")
    next(instance.WaitExecution(request, context))

    context.set_code.assert_called_once_with(grpc.StatusCode.INVALID_ARGUMENT)


@check_for_postgres
def test_job_deduplication_in_scheduling(instance, controller, context):
    scheduler = controller.execution_instance._scheduler

    action = remote_execution_pb2.Action(command_digest=command_digest,
                                         do_not_cache=False)
    action_digest = create_digest(action.SerializeToString())

    job_name1 = scheduler.queue_job_action(action, action_digest, skip_cache_lookup=True)

    operation_name1 = controller.execution_instance.register_job_peer(job_name1,
                                                                      context.peer())

    job_name2 = scheduler.queue_job_action(action, action_digest, skip_cache_lookup=True)

    operation_name2 = controller.execution_instance.register_job_peer(job_name2,
                                                                      context.peer())
    # The jobs are be deduplicated, but and operations are created
    assert job_name1 == job_name2
    assert operation_name1 != operation_name2

    if isinstance(scheduler.data_store, SQLDataStore):
        with scheduler.data_store.session() as session:
            query = session.query(models.Job)
            job_count = query.filter_by(name=job_name1).count()
            assert job_count == 1
            query = session.query(models.Operation)
            operation_count = query.filter_by(job_name=job_name1).count()
            assert operation_count == 2

    controller.execution_instance.unregister_operation_peer(operation_name1, context.peer())
    controller.execution_instance.unregister_operation_peer(operation_name2, context.peer())


@check_for_postgres
def test_job_reprioritisation(instance, controller, context):
    scheduler = controller.execution_instance._scheduler

    action = remote_execution_pb2.Action(command_digest=command_digest)
    action_digest = create_digest(action.SerializeToString())

    job_name1 = scheduler.queue_job_action(
        action, action_digest, skip_cache_lookup=True, priority=10)

    job = scheduler.data_store.get_job_by_name(job_name1)
    assert job.priority == 10

    job_name2 = scheduler.queue_job_action(
        action, action_digest, skip_cache_lookup=True, priority=1)

    assert job_name1 == job_name2
    job = scheduler.data_store.get_job_by_name(job_name1)
    assert job.priority == 1


@pytest.mark.parametrize("do_not_cache", [True, False])
@check_for_postgres
def test_do_not_cache_no_deduplication(do_not_cache, instance, controller, context):
    scheduler = controller.execution_instance._scheduler

    # The default action already has do_not_cache set, so use that
    job_name1 = scheduler.queue_job_action(action, action_digest, skip_cache_lookup=True)

    operation_name1 = controller.execution_instance.register_job_peer(job_name1,
                                                                      context.peer())

    action2 = remote_execution_pb2.Action(command_digest=command_digest,
                                          do_not_cache=do_not_cache)

    action_digest2 = create_digest(action2.SerializeToString())
    job_name2 = scheduler.queue_job_action(action2, action_digest2, skip_cache_lookup=True)

    operation_name2 = controller.execution_instance.register_job_peer(job_name2,
                                                                      context.peer())
    # The jobs are not be deduplicated because of do_not_cache,
    # and two operations are created
    assert job_name1 != job_name2
    assert operation_name1 != operation_name2

    if isinstance(scheduler.data_store, SQLDataStore):
        with scheduler.data_store.session() as session:
            job_count = session.query(models.Job).count()
            assert job_count == 2

            operation_count = session.query(models.Operation).count()
            assert operation_count == 2

    controller.execution_instance.unregister_operation_peer(operation_name1, context.peer())
    controller.execution_instance.unregister_operation_peer(operation_name2, context.peer())

def test_cancel_execution(instance, controller, context):
    # Verifying that the `Job.worker_completed_timestamp` field is set
    # for cancelled jobs.
    scheduler = controller.execution_instance._scheduler
    job_name = scheduler.queue_job_action(action, action_digest, skip_cache_lookup=True)

    operation_name = controller.execution_instance.register_job_peer(job_name,
                                                                     context.peer())

    scheduler._update_job_operation_stage(job_name, OperationStage.EXECUTING)
    scheduler.cancel_job_operation(operation_name)

    job = scheduler.data_store.get_job_by_name(job_name)
    assert job.worker_completed_timestamp is not None

    if isinstance(scheduler.data_store, SQLDataStore):
        with scheduler.data_store.session() as session:
            record = session.query(models.Job).filter_by(name=job_name).first()
            assert record is not None
            assert record.stage == job.OperationStage.COMPLETED.value
            assert record.operations
            assert all(op.done for op in record.operations)
            assert record.worker_completed_timestamp is not None

    controller.execution_instance.unregister_operation_peer(operation_name, context.peer())
