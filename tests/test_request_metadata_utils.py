# Copyright (C) 2020 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# pylint: disable=redefined-outer-name

from buildgrid._protos.build.bazel.remote.execution.v2 import remote_execution_pb2
from buildgrid.server.request_metadata_utils import extract_request_metadata
from buildgrid.settings import REQUEST_METADATA_HEADER_NAME
import grpc


class DummyServicerContext(grpc.ServicerContext):
    """Dummy context object passed to method implementations."""

    def invocation_metadata(self):
        return []

    def peer(self):
        raise NotImplementedError()

    def peer_identities(self):
        raise NotImplementedError()

    def peer_identity_key(self):
        raise NotImplementedError()

    def auth_context(self):
        raise NotImplementedError()

    def send_initial_metadata(self, initial_metadata):
        raise NotImplementedError()

    def set_trailing_metadata(self, trailing_metadata):
        raise NotImplementedError()

    def abort(self, code, details):
        raise NotImplementedError()

    def set_code(self, code):
        raise NotImplementedError()

    def set_details(self, details):
        raise NotImplementedError()

    def abort_with_status(self, status):
        raise NotImplementedError()

    def add_callback(self, callback):
        raise NotImplementedError()

    def cancel(self):
        raise NotImplementedError()

    def is_active(self):
        raise NotImplementedError()

    def time_remaining(self):
        raise NotImplementedError()


class DummyServicerContextWithMetadata(DummyServicerContext):
    def invocation_metadata(self):
        request_metadata = remote_execution_pb2.RequestMetadata()
        request_metadata.tool_details.tool_name = 'tool-name'
        request_metadata.tool_details.tool_version = '1.0'

        request_metadata.action_id = '920ea86d6a445df893d0a39815e8856254392ce40e5957f167af8f16485916fb'
        request_metadata.tool_invocation_id = 'cec14b04-075e-4f47-9c24-c5e6ac7f9827'
        request_metadata.correlated_invocations_id = '9f962383-25f6-43b3-886d-56d761ac524e'

        from collections import namedtuple
        metadata_entry = namedtuple('metadata_entry', ('key', 'value'))

        return [metadata_entry(REQUEST_METADATA_HEADER_NAME,
                               request_metadata.SerializeToString())]


def test_extract_request_metadata():
    context = DummyServicerContextWithMetadata()
    metadata = extract_request_metadata(context)

    assert metadata.tool_details.tool_name == 'tool-name'
    assert metadata.tool_details.tool_version == '1.0'
    assert metadata.action_id == '920ea86d6a445df893d0a39815e8856254392ce40e5957f167af8f16485916fb'
    assert metadata.tool_invocation_id == 'cec14b04-075e-4f47-9c24-c5e6ac7f9827'
    assert metadata.correlated_invocations_id == '9f962383-25f6-43b3-886d-56d761ac524e'


def test_extract_request_metadata_context_with_no_metadata():
    context = DummyServicerContext()
    metadata = extract_request_metadata(context)

    assert metadata == remote_execution_pb2.RequestMetadata()
