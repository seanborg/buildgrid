# Copyright (C) 2020 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from grpc import ServicerContext

from buildgrid._protos.build.bazel.remote.execution.v2.remote_execution_pb2 import RequestMetadata
from buildgrid.settings import REQUEST_METADATA_HEADER_NAME


def printable_request_metadata(context: ServicerContext) -> str:
    """Given a `grpc.ServicerContext` object, return a human-readable
    representation of its `RequestMetadata`.

    Args:
        context (grpc.ServicerContext): Context for a RPC call.

    Returns:
        A string with the metadata contents.
    """
    metadata = extract_request_metadata(context)

    if metadata.tool_details:
        tool_name = metadata.tool_details.tool_name
        tool_version = metadata.tool_details.tool_version
    else:
        tool_name = tool_version = ''

    return (
        f'tool_name="{tool_name}", tool_version="{tool_version}", '
        f'action_id="{metadata.action_id}", '
        f'tool_invocation_id="{metadata.tool_invocation_id}", '
        f'correlated_invocations_id="{metadata.correlated_invocations_id}"'
    )


def extract_request_metadata(context) -> RequestMetadata:
    """Given a `grpc.ServicerContext` object, extract the RequestMetadata
    header values if they are present. If they were not provided,
    returns an empty message.

    Args:
        context (grpc.ServicerContext): Context for a RPC call.

    Returns:
        A `RequestMetadata` proto. If the metadata is not defined in the
        request, the message will be empty.
    """
    invocation_metadata = context.invocation_metadata()
    request_metadata_entry = next((entry for entry in invocation_metadata
                                   if entry.key == REQUEST_METADATA_HEADER_NAME),
                                  None)

    request_metadata = RequestMetadata()
    if request_metadata_entry:
        request_metadata.ParseFromString(request_metadata_entry.value)
    return request_metadata
