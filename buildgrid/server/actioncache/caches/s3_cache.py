# Copyright (C) 2019 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""
S3 Action Cache
==================

Implements an Action Cache using S3 to store cache entries.

"""

import io
import logging
from typing import Optional

import boto3
from botocore.exceptions import ClientError

from buildgrid._exceptions import (
    NotFoundError,
    StorageFullError
)
from buildgrid._protos.build.bazel.remote.execution.v2.remote_execution_pb2 import (
    ActionResult,
    Digest,
)
from buildgrid.server.actioncache.caches.action_cache_abc import ActionCacheABC
from buildgrid.server.cas.storage.storage_abc import StorageABC
from buildgrid._enums import ActionCacheEntryType


class S3ActionCache(ActionCacheABC):
    def __init__(self, storage: StorageABC, allow_updates: bool=True,
                 cache_failed_actions: bool=True,
                 entry_type: Optional[ActionCacheEntryType]=ActionCacheEntryType.ACTION_RESULT_DIGEST,
                 migrate_entries: Optional[bool]=False,
                 bucket: Optional[str]=None,
                 endpoint: Optional[str]=None, access_key: Optional[str]=None,
                 secret_key: Optional[str]=None):
        """ Initialises a new ActionCache instance using S3 to persist the action cache.

        Args:
            storage (StorageABC): storage backend instance to be used to store ActionResults.
            allow_updates (bool): allow the client to write to storage
            cache_failed_actions (bool): whether to store failed actions in the Action Cache
            entry_type (ActionCacheEntryType): whether to store ActionResults or their digests.
            migrate_entries (bool): if set, migrate entries that contain a value with
                a different `EntryType` to `entry_type` as they are accessed
                (False by default).

            bucket (str): Name of bucket
            endpoint (str): URL of endpoint.
            access-key (str): S3-ACCESS-KEY
            secret-key (str): S3-SECRET-KEY
        """
        ActionCacheABC.__init__(self, storage=storage, allow_updates=allow_updates)
        self._logger = logging.getLogger(__name__)

        self._entry_type = entry_type
        self._migrate_entries = migrate_entries

        self._cache_failed_actions = cache_failed_actions
        self._bucket = bucket

        self._s3cache = boto3.resource('s3', endpoint_url=endpoint, aws_access_key_id=access_key,  # type: ignore
                                       aws_secret_access_key=secret_key)

    # --- Public API ---
    @property
    def allow_updates(self) -> bool:
        return self._allow_updates

    def get_action_result(self, action_digest: Digest) -> ActionResult:
        """Retrieves the cached ActionResult for the given Action digest.

        Args:
            action_digest: The digest to get the result for

        Returns:
            The cached ActionResult matching the given key or raises
            NotFoundError.
        """
        action_result = self._get_action_result(action_digest)
        if action_result is not None and self._action_result_blobs_still_exist(action_result):
            return action_result

        if self._allow_updates:
            self._logger.debug(f"Removing {action_digest.hash}/{action_digest.size_bytes} "
                               "from cache due to missing blobs in CAS")
            self._delete_key_from_cache(action_digest)

        raise NotFoundError(f"Key not found: {action_digest.hash}/{action_digest.size_bytes}")

    def update_action_result(self, action_digest: Digest,
                             action_result: ActionResult) -> None:
        """Stores the result in cache for the given key.

        Args:
            action_digest (Digest): digest of Action to update
            action_result (ActionResult): ActionResult to store.
        """
        if not self._allow_updates:
            raise NotImplementedError("Updating cache not allowed")

        if self._cache_failed_actions or action_result.exit_code == 0:
            action_result_digest = self._storage.put_message(action_result)

            if self._entry_type == ActionCacheEntryType.ACTION_RESULT_DIGEST:
                self._update_cache_key(action_digest, action_result_digest.SerializeToString())
            else:
                self._update_cache_key(action_digest, action_result.SerializeToString())

            self._logger.info(
                f"Result cached for action [{action_digest.hash}/{action_digest.size_bytes}]")

    # --- Private API ---
    def _get_action_result(self, digest: Digest) -> Optional[ActionResult]:
        """Get an `ActionResult` from the cache.

        If present, returns the `ActionResult` corresponding to the given digest.
        Otherwise returns None.

        When configured to do so, if the value stored in the entry in S3 contains
        a different type, it will convert it to `entry_type`.

        Args:
            digest: Action digest to get the associated ActionResult digest for

        Returns:
            The `ActionResult` or None if the digest is not present
        """
        value_in_cache = self._get_value_from_cache(digest)
        if not value_in_cache:
            return None

        # Attempting to parse the entry as a `Digest` first:
        action_result_digest = Digest.FromString(value_in_cache)
        if len(action_result_digest.hash) == len(digest.hash):
            # The cache contains the `Digest` of the `ActionResult`:
            action_result = self._storage.get_message(action_result_digest,
                                                      ActionResult)

            # If configured, update the entry to contain an `ActionResult`:
            if self._entry_type == ActionCacheEntryType.ACTION_RESULT and self._migrate_entries:
                self._logger.debug(f"Converting entry for {digest.hash}/{digest.size_bytes} "
                                   "from Digest to ActionResult")
                self._update_cache_key(digest, action_result.SerializeToString())

        else:
            action_result = ActionResult.FromString(value_in_cache)

            # If configured, update the entry to contain a `Digest`:
            if self._entry_type == ActionCacheEntryType.ACTION_RESULT_DIGEST and self._migrate_entries:
                self._logger.debug(f"Converting entry for {digest.hash}/{digest.size_bytes} "
                                   "from ActionResult to Digest")
                action_result_digest = self._storage.put_message(action_result)
                self._update_cache_key(digest, action_result_digest.SerializeToString())

        return action_result

    def _get_value_from_cache(self, digest: Digest) -> Optional[bytes]:
        """Get the bytes stored in cache for the given Digest.

            Args:
                digest: Action digest to get the associated bytes in S3

            Returns:
                bytes or None if the digest is not present
        """

        try:
            obj = self._s3cache.Object(self._bucket, self._get_key(digest))
            return obj.get()['Body'].read()
        except ClientError as e:
            if e.response['Error']['Code'] not in ['404', 'NoSuchKey']:
                raise
            return None

    def _update_cache_key(self, digest: Digest, value: bytes) -> None:
        try:
            obj = self._s3cache.Object(self._bucket, self._get_key(digest))
            obj.upload_fileobj(io.BytesIO(value))
        except ClientError as error:
            if error.response['Error']['Code'] == 'QuotaExceededException':
                raise StorageFullError("ActionCache S3 Quota Exceeded.") from error
            raise error

    def _delete_key_from_cache(self, digest: Digest) -> None:
        """Remove an entry from the ActionCache

        Args:
            digest: entry to remove from the ActionCache

        Returns:
            None
        """
        if not self._allow_updates:
            raise NotImplementedError("Updating cache not allowed")

        obj = self._s3cache.Object(self._bucket, self._get_key(digest))
        obj.delete()

    @staticmethod
    def _get_key(digest: Digest) -> str:
        """
        Given a `Digest`, returns the key used to store its
        corresponding entry in S3.
        """
        return f'{digest.hash}_{digest.size_bytes}'
