# Copyright (C) 2020 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""
PostgreSQLDelegate
==================

Extra functionality for the SQL index when using a PostgreSQL backend.

"""
from datetime import datetime
from typing import List

from sqlalchemy.dialects.postgresql import insert
from sqlalchemy.orm.session import Session as SessionType

from buildgrid._protos.build.bazel.remote.execution.v2.remote_execution_pb2 import Digest
from buildgrid.server.persistence.sql.models import IndexEntry


class PostgreSQLDelegate:

    @staticmethod
    def _save_digests_to_index(digests: List[Digest], session: SessionType) -> None:
        index_table = IndexEntry.__table__
        update_time = datetime.utcnow()
        new_rows = [{
            'digest_hash': digest.hash,
            'digest_size_bytes': digest.size_bytes,
            'accessed_timestamp': update_time
        } for digest in digests]
        stmt = insert(index_table).values(new_rows).on_conflict_do_update(
            index_elements=['digest_hash'],
            set_={'accessed_timestamp': update_time}
        )
        session.execute(stmt)
