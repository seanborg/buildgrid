.. _about:

About
=====


.. _what-is-it:

What is BuildGrid?
------------------

BuildGrid is a Python remote execution service which implements Google's
`Remote Execution API`_ and the `Remote Workers API`_. The project's goal is to
be able to execute build jobs remotely on a grid of computers in order to
massively speed up build times. Workers on the grid should be able to run with
different environments. It works with clients such as `Bazel`_,
`BuildStream`_ and `RECC`_, and is designed to be able to work with any client
that conforms to the above API protocols.

BuildGrid is designed to work with any worker conforming to the `Remote Workers API`_
specification. Workers actually execute the jobs on the backend while BuildGrid does
the scheduling and storage. The `BuildBox`_ ecosystem provides a suite of workers and
sandboxing tools that work with the Workers API and can be used with BuildGrid.

.. _Remote Execution API: https://github.com/bazelbuild/remote-apis
.. _Remote Workers API: https://docs.google.com/document/d/1s_AzRRD2mdyktKUj2HWBn99rMg_3tcPvdjx3MPbFidU/edit#heading=h.1u2taqr2h940
.. _BuildStream: https://wiki.gnome.org/Projects/BuildStream
.. _Bazel: https://bazel.build
.. _RECC: https://gitlab.com/bloomberg/recc
.. _BuildBox: https://buildgrid.gitlab.io/buildbox/buildbox-home/


.. _whats-going-on:

What's Going On?
----------------

Over the last few weeks we've added support for versions 2.1 and 2.2 of the
Remote Execution API. When used with a client which supports version 2.2,
BuildGrid now skips fetching the Command message for an Action except when
strictly necessary, thanks to platform properties being present in the Action
directly now. This removes a CAS hit and reduces the time spent processing
an Action before enqueuing it.

Continuing in that vein, we've also made BuildGrid send Action messages in the
payload field of Leases that get sent to workers. Previously we sent the digest
of the Action, so the worker had to fetch the Action that we'd already fetched
in the server. This removes another CAS hit, which lets the worker start executing
the Action faster.

We've also optimised our persistent Action Cache implementations, by storing the
actual ActionResult in the cache rather than a digest to the result in CAS. This
removes a CAS request when getting a cache hit.

Some metrics bugs have been fixed, including the empty timer metrics for measuring
the duration of Execute, WaitExecution, and ByteStream Read requests. We've also
split the metrics for the !with-cache-storage CAS backend by request type, to
allow more detailed reasoning about how the cache is behaving.

Additionally, we've added a way to set the gRPC channel options for the remote
CAS and Action Cache backends.

We've also added metrics to the recent authorization work around fetching and
using JWKs from a remote location to authenticate JWTs.

Next, we're working on further optimising the ActionCache and CAS backends, as
well as adding support for Lease requirements, to allow workers to pre-check that
they can actually execute an Action as expected.

See our `release notes`_ for the latest changes/updates.

.. _release notes: https://gitlab.com/BuildGrid/buildgrid/-/blob/master/release_notes.rst

.. _readme-getting-started:

Getting started
---------------

Please refer to the `documentation`_ for `installation`_ and `usage`_
instructions, plus guidelines for `contributing`_ to the project.

.. _contributing: https://buildgrid.build/developer/contributing.html
.. _documentation: https://buildgrid.build/
.. _installation: https://buildgrid.build/user/installation.html
.. _usage: https://buildgrid.build/user/using.html


.. _about-resources:

Resources
---------

- `Homepage`_
- `GitLab repository`_
- `Bug tracking`_
- `Mailing list`_
- `Slack channel`_ [`invite link`_]
- `FAQ`_

.. _Homepage: https://buildgrid.build/
.. _GitLab repository: https://gitlab.com/BuildGrid/buildgrid
.. _Bug tracking: https://gitlab.com/BuildGrid/buildgrid/boards
.. _Mailing list: https://lists.buildgrid.build/cgi-bin/mailman/listinfo/buildgrid
.. _Slack channel: https://buildteamworld.slack.com/messages/CC9MKC203
.. _invite link: https://bit.ly/2SG1amT
.. _FAQ: https://buildgrid.build/user/faq.html
