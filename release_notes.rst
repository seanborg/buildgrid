.. _release-notes:

Release Notes:

Version tag `0.0.2`_: See `diff to 0.0.1`_
---------

- CAS metrics
-  Add method to bulk delete blobs from S3Storage
-  Add a method to bulk delete indexed CAS entries
-  Skeleton implementation for S3 CAS Expiry
-  Add a method to mark CAS index entries as deleted
-  Add a `deleted` column to the indexed CAS table

.. _0.0.2: https://gitlab.com/BuildGrid/buildgrid/-/tree/0.0.2
.. _diff to 0.0.1: https://gitlab.com/BuildGrid/buildgrid/-/compare/b248abc578ad9e0892175eb7e3c7cdc0ffb40aba...0.0.2
